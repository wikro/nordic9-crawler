FROM node:current

RUN mkdir -p /n9c/node_modules
WORKDIR /n9c

COPY index.js .
COPY package.json .
COPY settings.json .

RUN npm install

USER nobody
EXPOSE 3000
CMD [ "node", "index.js" ]
