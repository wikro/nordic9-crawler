# 1. Contents

1. [Contents](#markdown-header-1-contents)
2. [Introduction](#markdown-header-2-introduction)
3. [Installing and running](#markdown-header-3-installing-and-running)
    1. [Using a docker image, pre-built on Centos 7](#markdown-header-31-using-a-docker-image-pre-built-on-centos-7)
    2. [Using a docker image, from Docker Hub](#markdown-header-32-using-a-docker-image-from-docker-hub)
    3. [Using source, building and running manually](#markdown-header-33-using-source-building-and-running-manually)
        1. [Getting the source](#markdown-header-331-getting-the-source)
        2. [Building and running with Docker](#markdown-header-332-building-and-running-with-docker)
        3. [Running the app in the terminal](#markdown-header-333-running-the-app-in-the-terminal)
        4. [Running the app with PM2](#markdown-header-334-running-the-app-with-pm2)
4. [Subscribing to feed](#markdown-header-4-subscribing-to-feed)
    1. [Subscribing using G Suite Calendar](#markdown-header-41-subscribing-using-g-suite-calendar)

# 2. Introduction

`nordic9-crawler` is a [`node`](https://nodejs.org/) app that scrapes https://nordic9.com/countries/se-sweden/events for events at a set interval and publishes them as an [iCalendar](https://en.wikipedia.org/wiki/ICalendar) feed, which can then be subscribed to using G Suite Calendar,  for example.

# 3. Installing and running

## 3.1. Using a docker image, pre-built on Centos 7

Requires [`docker`](https://www.docker.com/) to be installed and set up to auto-start on system boot.

```bash
# Download the image
wget https://hjkl.se/nordic9-crawler/releases/nordic9-crawler-latest-image.zip

# Load the image
sudo docker load -i nordic9-crawler-latest-image.zip

# Start app running on localhost, port 3000. To change port, replace `3000:3000` with `8888:3000` to run on port 8888 on the localhost instead.
sudo docker run -d -p 3000:3000 wikro/nordic9-crawler
```

## 3.2. Using a docker image, from Docker Hub

Requires [`docker`](https://www.docker.com/) to be installed and set up to auto-start on system boot.

```bash
# Start app running on localhost, port 3000. To change port, replace `3000:3000` with `8888:3000` to run on port 8888 on the localhost instead.
sudo docker run --rm -p 3000:3000 wikro/nordic9-crawler
```

## 3.3. Using source, building and running manually

### 3.3.1. Getting the source

```bash
# Download source using `git` with `https`
git clone https://bitbucket.org/wikro/nordic9-crawler.git

# Or using `git` with `ssh`
git clone git@bitbucket.org:wikro/nordic9-crawler.git
```

Edit the `settings.json` file, adjust the settings and save before running or building the app.

It is **strongly** recommended that you change the `domain` setting to your own domain or subdomain.

The below configuration is the default settings, should `settings.json` be missing.

```javascript
{
  "domain": "n9c.js", // Change to your own domain or subdomain
  "interval": 60,     // The interval in minutes at which the app scrapes events
  "port": 3000       // The port the app will listen for request on
}
```

### 3.3.2. Building and running with Docker

Requires [`docker`](https://www.docker.com/) to be installed and set up to auto-start on system boot.

```bash
# Enter app directory
cd nordic9-crawler

# Build the image
sudo docker build -t wikro/nordic9-crawler:latest .

# If you plan on exporting and transfering the image to another machine, otherwise skip
sudo docker save -o nordic9-crawler-image.zip wikro/nordic9-crawler

# If you previously exported to an image, and want to run it on a machine it's been transfered to, otherwise skip
sudo docker load -i nordic9-crawler-image.zip

# This starts the app running on localhost, port 3000. To change port, replace `3000:3000` with `8888:3000` to run on port 8888 on the localhost instead.
sudo docker run -d -p 3000:3000 wikro/nordic9-crawler
```

### 3.3.3. Running the app in the terminal

`nordic9-crawler` requires [`node`](https://nodejs.org/) and [`npm`](https://www.npmjs.com/) to install and run.

```bash
# Enter app directory
cd nordic9-crawler

# Install dependencies
npm install

# To run the app in the terminal using `node`
node index.js

# Or using `npm`
npm start
```

### 3.3.4. Running the app with PM2

`nordic9-crawler` requires [`node`](https://nodejs.org/) and [`npm`](https://www.npmjs.com/) to install and run.

Here's and example of how to use `pm2` to start the app as a service, running in the background.

**Note**: The `pm2` process will be owned by the user executing the command. In the instructions below, `sudo` is used and PM2 will run as `root`.

```bash
# Enter app directory
cd nordic9-crawler

# Install dependencies
npm install

# Install PM2 using npm
sudo npm install -g pm2

# Install startup scripts for the detected OS
sudo pm2 startup

# Go up one directory in the file tree
cd ..

# Start the app
sudo pm2 start nordic9-crawler

# Save the current list of apps to automatically start with pm2
sudo pm2 save

# Verify and check status.
sudo pm2 list

#Further help managing the service.
sudo pm2 -h
```

# 4. Subscribing to feed

The the iCalendar feed will be published at http://yourhost:3000/n9c.ics by default. This can be adjusted to a friendlier address if the `port` setting is changed to `80`, in which case the feed will be published at http://yourhost/n9c.ics. 

Use this URL when subscribing to the feed using your client of choice.

The app can run like this or be put behind a reverse proxy like [`nginx`](https://www.nginx.com/) to allow for better control, SSL support and other things not supported by the app natively.

## 4.1. Subscribing using G Suite Calendar

1. Go to https://calendar.google.com and login using your G Suite credentials.
2. Click the **+** above **My calendars**
3. Click **From URL**
4. Enter the URL to the iCalendar feed
5. Click **Add calendar**

