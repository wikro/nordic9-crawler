const app = require('express')();
const { JSDOM } = require('jsdom');
const info = require('./package.json');
let icalCache = '';
let metricsCache = '';
let settings = {};
try {
  settings = require('./settings.json');
} catch (e) {
  console.warn(`Error loading "settings.json", defaults will be used: ${e.code}`);
}

console.log(`Starting up ${info.name} v${info.version}`);

const mapMetrics = (mapping) => (metrics) => {
  const mappedMetrics = Object.keys(metrics).map((metric) => {
    return mapping[metric](metrics[metric]);
  }).join('\n') + '\n';

  console.debug(`-----Begin mappedMetrics-----\n${mappedMetrics}\n-----End mappedMetrics-----`);
  return mappedMetrics;
};

const promMetric = (type) => (name) => (help) => (data) => {
  let metricsBlock = '';

  if (['counter', 'gauge', 'histogram'].includes(type) && help.length > 0) {
    if (name.match(/[a-zA-Z_][a-zA-Z0-9_]*/).length > 0) {

      const customName = `custom_n9c_${name}`;

      const head = [
        `# HELP ${customName} ${help}`,
        `# TYPE ${customName} ${type}`
      ].join('\n');

      const metrics = data.map((metric) => {
        const value = metric.value;

        const labels = Object.keys(metric.labels || {}).map((label) => {
          return `${label}="${metric.labels[label]}"`;
        }).join(',');

        return `${customName}{${labels}} ${value}`
      }).join('\n');

      metricsBlock = `${head}\n${metrics}`;
    }
  }

  console.debug(`-----Begin metricsBlock-----\n${metricsBlock}\n-----End metricsBlock-----`);
  return metricsBlock;
};

const buildMetrics = mapMetrics({
  fetched_site_total: promMetric('counter')('fetched_site_total')('Number of times app has fetched target site'),
  parsed_events_total: promMetric('counter')('parsed_events_total')('Number of events parsed')
});

const metrics = {};

metrics.fetched_site_total = [{
  labels: {status: '200'},
  value: 0
},
{
  labels: {status: '400'},
  value: 0
},
{
  labels: {status: '500'},
  value: 0
},
{
  labels: {status: '000'},
  value: 0
}];

const fetched_site_total_200 = metrics.fetched_site_total[0];
const fetched_site_total_400 = metrics.fetched_site_total[1];
const fetched_site_total_500 = metrics.fetched_site_total[2];
const fetched_site_total_000 = metrics.fetched_site_total[3];

metrics.parsed_events_total = [{
  value: 0
}];

const parsed_events_total_all = metrics.parsed_events_total[0];

const formatDate = (date) => {
  const formatted = date.split('+')[0].replace(/[-:]/g, '') + 'Z';
  console.debug(`Formatting date: '${date}' => '${formatted}'`);
  return formatted;
};

const buildICalendar = (vevents) => {
  const ical = [
    'BEGIN:VCALENDAR',
    'VERSION:2.0',
    'PRODID:-//hacksw/handcal//NONSGML v1.0//EN',
    vevents,
    'END:VCALENDAR'
  ].filter(line => line.length > 0) // filter out empty lines
  .join('\n') // join into one string, separate each line with a line break
  .replace(',', '\,') // escape all ',' characters
  .replace(/(.{75})/g, '$1\n ') + '\n'; // every 75 characters, insert a line break and add a space on the new line after the break. Lines have a 75 character line limit, and any line longer than that has to be broken after 75 characters and continued on a new line with a leading space, as described in RFC 5545 3.1. Content Lines

  console.debug(`-----Begin ical-----\n${ical}\n-----End ical-----`);
  return ical; // new line at the end of file signals EOF
};

const createUid = (date) => (title, ...rec) => {
  const uid = rec[0] || ''; // default to empty string
  const ti = rec[1] || 0; // ti = title index, start at index 0 of title string by default
  const ft = rec[2] || title.replace(/[\s\n]+/g, ''); // ft = formatted title, default to remove white space and new lines from title string

  if (title.length <= 0 || date.length <= 0) {
    return '';
  }

  if (uid.length >= 16 || ti >= title.length) {
    // ex: 20200908T160000Z-7811111410010599@n9c.js
    const fullUid = `${date}-${uid.slice(0, 16)}@${settings.domain || 'n9c.js'}`;
    console.debug(`UID created: '${date} ${title}' => ${fullUid}`);
    return fullUid;
  }

  return createUid(date)(title, uid + title.charCodeAt(ti), ti + 1, ft);
};


const buildVevents = (eventsDom, ...rec) => {
  const vevents = rec[0] || '';
  const di = rec[1] || 0; // di = eventsDom index, start at index 0 of eventsDom array by default

  if (di >= eventsDom.length || eventsDom.length <= 0) {
    return vevents;
  }

  const e = eventsDom[di];

  const url = e.querySelector('[itemprop="url"]').getAttribute('href');
  const title = e.querySelector('[itemprop="url"]').getAttribute('title');
  const locationName = e.querySelector('[itemprop="location"] > [itemprop="name"]').textContent;
  const locationAddress = e.querySelector('[itemprop="location"] > [itemprop="address"]').getAttribute('content');
  const categories = e.querySelector('[itemprop="description"]').textContent.trim().split(' ').join(', ');
  const startDate = formatDate(e.querySelector('[itemprop="startDate"]').getAttribute('content'));
  const endDate = formatDate(e.querySelector('[itemprop="endDate"]').getAttribute('content'));

  const uid = createUid(startDate)(title);
  const description = `${categories}\\n\\n${url}`;

  const vevent = [
    'BEGIN:VEVENT',
    `DTSTART:${startDate}`,
    `DTEND:${endDate}`,
    `DTSTAMP:${startDate}`,
    `UID:${uid}`,
    `LOCATION:${locationAddress}`,
    `SUMMARY:${title}`,
    `DESCRIPTION:${description}`,
    'END:VEVENT'
  ].join('\n');

  console.debug(`-----Begin vevent-----\n${vevent}\n-----End vevent-----`);
  console.info(`Updating metrics; parsed_events_total_all.value++`);
  parsed_events_total_all.value++;

  return buildVevents(eventsDom, `${vevents}${di > 0 ? '\n' : ''}${vevent}`, di + 1);
};

const loop = () => {
  JSDOM.fromURL('https://nordic9.com/countries/se-sweden/events').then((dom) => {
    console.info('Target site has been fetched');

    console.info('Building and caching iCalendar');
    icalCache = buildICalendar(buildVevents(dom.window.document.querySelectorAll('section.section-news > article.event-box')));

    console.info('Updating metrics; fetched_site_total_success.value++');
    fetched_site_total_200.value++;
  })
  .catch((e) => {

    let statusCode = '000';
    let error = 'Unknown error';

    if (e.response) {
      statusCode = e.response.statusCode.toString();
      error = `${statusCode} ${e.response.statusMessage}`;
    } else {
      error = `${e.message}`;
    }

    console.error(`Failed to fetch target site: ${error}`);

    console.error('Building empty iCalendar');
    icalCache = buildICalendar(buildVevents([]));
  
    switch(statusCode.charAt(0)) {
      case '4':
        console.info('Updating metrics; fetched_site_total_400.value++');
        fetched_site_total_400.value++;
        break;
      case '5':
        console.info('Updating metrics; fetched_site_total_500.value++');
        fetched_site_total_500.value++;
        break;
      default:
        console.info('Updating metrics; fetched_site_total_000.value++');
        fetched_site_total_000.value++;
    }
        
  }).finally(() => {
    console.info('Building and caching metrics');
    metricsCache = buildMetrics(metrics);
  });

  const interval = settings.interval || 60; // Loop every N minutes
  const timeout = interval * 60 * 1000; // Interval * 1 minute in milliseconds
  const now = new Date();
  const delay = timeout - (now % timeout);

  console.info(`Timeout in ${delay} ms`);
  setTimeout(loop, delay);
};

app.get('/n9c.ics', (req, res) => res.set('content-type', 'text/calendar').set('icalCache-control', 'no-icalCache, no-store, max-age=0, must-revalidate').set('pragma', 'no-icalCache').send(icalCache));

app.get('/metrics', (req, res) => res.set('content-type', 'text/plain').send(metricsCache));

app.listen((settings.port || 3000), () => {
    console.log(`Listening on *:${(settings.port || 3000)}`)
    loop();
  })
  .on('error', (e) => {
    console.error(`Failed to start http server; shutting down: ${e}`);
  });
